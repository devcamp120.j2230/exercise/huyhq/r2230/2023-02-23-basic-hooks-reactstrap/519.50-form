import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import FormRegistration from './component/FormRegistration';

function App() {
  return (
    <div className="container">
      <FormRegistration/>
    </div>
  );
}

export default App;
