
import { Button, Col, Form, FormGroup, Input, Label, Row } from 'reactstrap';

const FormRegistration = () => {
    return (
        <>
            <Row>
                <Col className="text-center"><h1>Registration form</h1></Col>
            </Row>
            <Form className="row">
                <Row>
                    <Col xs='6'>
                        <FormGroup row>
                            <Label sm={2} >
                                Firstname
                            </Label>
                            <Col sm={10}>
                                <Input placeholder="Firstname" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                Lastname
                            </Label>
                            <Col sm={10}>
                                <Input placeholder="Lastname" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                Birthday
                            </Label>
                            <Col sm={10}>
                                <Input placeholder="Birthday" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                Gender
                            </Label>
                            <Col sm={10} className="row">
                                <Col sm={6}>
                                    <Input name="gender" id="gender1" type="radio" />
                                    <Label for='gender1'>male</Label>
                                </Col>
                                <Col sm={6}>
                                    <Input name="gender" id="gender2" type="radio" />
                                    <Label for='gender2'>female</Label>
                                </Col>
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col xs='6'>
                        <FormGroup row>
                            <Label sm={2} >
                                Passport
                            </Label>
                            <Col sm={10}>
                                <Input placeholder="Passport" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                Email
                            </Label>
                            <Col sm={10}>
                                <Input placeholder="Email" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                Country
                            </Label>
                            <Col sm={10}>
                                <Input placeholder="Country" type="text" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2} >
                                Region
                            </Label>
                            <Col sm={10}>
                                <Input placeholder="Region" type="text" />
                            </Col>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <FormGroup row>
                        <Label sm={1} >
                            Subject
                        </Label>
                        <Col sm={11}>
                            <textarea className="form-control" placeholder="Subject" rows="5" />
                        </Col>
                    </FormGroup>
                </Row>
                <Row>
                    <Col className="text-end mt-3">
                        <Button color="primary" className="me-3">Check Data</Button>
                        <Button color="info">Send</Button>
                    </Col>
                </Row>
            </Form>
        </>
    )
};

export default FormRegistration;